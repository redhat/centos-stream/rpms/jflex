#!/bin/bash
# Author: Mikolaj Izdebski <mizdebsk@redhat.com>
. /usr/share/beakerlib/beakerlib.sh

rlJournalStart

  rlPhaseStartTest "check for presence of jflex command"
    rlAssertRpm jflex
    rlAssertBinaryOrigin jflex jflex
  rlPhaseEnd

  rlPhaseStartTest "display jflex version"
    rlRun -s "jflex --version"
    rlAssertGrep "This is JFlex" $rlRun_LOG
  rlPhaseEnd

rlJournalEnd
rlJournalPrintText
